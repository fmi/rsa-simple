import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;

public class Main {

	private static BigDecimal sum;

	private static class Callback implements SumThread.ResultCallback {

		@Override
		public void result(BigDecimal result) {
			addToSum(result);
		}

	}

	public static void main(String[] args) {
		List<String> argsList = Arrays.asList(args);

		long p = -1;
		int t = -1;
		String output = "output.txt";

		int index = argsList.indexOf("-p");
		if (index > -1 && index + 1 < argsList.size()) {
			try {
				p = Long.decode(argsList.get(index + 1));
			} catch (NumberFormatException e) {
				System.out.println("Invalid value for p");
				System.exit(1);
			}
		} else {
			System.out.println("Invalid value for p");
			System.exit(1);
		}

		index = argsList.indexOf("-t");
		if (index > -1 && index + 1 < argsList.size()) {
			try {
				t = Integer.decode(argsList.get(index + 1));
			} catch (NumberFormatException e) {
				System.out.println("Invalid value for t");
				System.exit(2);
			}
		} else {
			System.out.println("Invalid value for t");
			System.exit(2);
		}

		index = argsList.indexOf("-o");
		if (index > -1 && index + 1 < argsList.size()) {
			output = argsList.get(index + 1);
		}

		int precision = (int) p * 2;
		sum = BigDecimal.ZERO;

		long startTime = System.currentTimeMillis();
		Thread[] threads = new Thread[t];
		for (int i = 0; i < t; i++) {
			threads[i] = new SumThread(i, p, t, new Callback(), precision);
			threads[i].start();
		}

		try {
			for (Thread thread : threads) {
				thread.join();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		long elapsedTime = System.currentTimeMillis() - startTime;

		System.out.println(String.format(
				"Elapsed time: %d minutes, %d seconds, %d milliseconds",
				(int) ((elapsedTime / (1000 * 60)) % 60),
				(int) (elapsedTime / 1000) % 60, (int) (elapsedTime % 1000)));

		sum = sum.round(new MathContext(precision - 1, RoundingMode.DOWN));
		try {
			FileWriter writer = new FileWriter(output);
			writer.write(sum.toString());
			writer.close();
			System.out.println("Result written in " + output);
		} catch (IOException e) {
			System.out.println("Could not write result in file " + output);
		}

	}

	private static synchronized void addToSum(BigDecimal value) {
		sum = sum.add(value);
	}

}
