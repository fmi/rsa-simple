import java.math.BigDecimal;
import java.math.RoundingMode;

public class SumThread extends Thread {

	public interface ResultCallback {
		void result(BigDecimal result);
	}

	private long current;
	private long to;
	private long step;
	private int precision;
	private BigDecimal sum;
	private ResultCallback callback;

	public SumThread(long from, long to, long step, ResultCallback callback,
			int precision) {
		current = from;
		this.to = to;
		this.step = step;
		this.callback = callback;
		this.precision = precision;
	}

	@Override
	public void run() {

		sum = BigDecimal.ZERO;
		while (current <= to) {
			BigDecimal dividend = new BigDecimal(current).multiply(
					new BigDecimal(2)).add(BigDecimal.ONE);

			BigDecimal divisor = factorial(2 * current);

			BigDecimal result = dividend.divide(divisor, precision,
					RoundingMode.HALF_UP);
			sum = sum.add(result);
			current += step;
		}
		if (callback != null) {
			callback.result((sum));

		}
	}

	public static BigDecimal factorial(long n) {
		BigDecimal f = BigDecimal.ONE;
		BigDecimal g = BigDecimal.valueOf(n);

		while (g.compareTo(BigDecimal.ONE) > 0) {
			f = f.multiply(g);
			g = g.subtract(BigDecimal.ONE);
		}
		return f;
	}

}
